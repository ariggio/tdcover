var express = require('express');
var router = express.Router();

const fs = require('fs');

const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');


var petalStatus = Object.freeze({
    petalNOpen: 1 << 0,
    petalNClose: 1 << 1,
    petalSOpen: 1 << 2,
    petalSClose: 1 << 3,
    petalEOpen: 1 << 4,
    petalEClose: 1 << 5,
    petalWOpen: 1 << 6,
    petalWClose: 1 << 7
});

const port = new SerialPort('/dev/ttyACM0',
        {
            autoOpen: false,
            baudRate: 115200
        },
        function (e) {
            console.log("Creating a serial port");

            if (e) {
                console.log("An error occurred trying to create the serial port\n", e);
            }
        });

port.open(function (e) {
    console.log("Try opening serial port");

    if (e) {
        console.log("An error occurred trying to open the serial port\n", e);
    }
});

const parser = port.pipe(new Readline({delimiter: '\r\n'}));


console.log("tdc_serial_comm.js called");

// Check port is ok
port.on('error', function (e) {
    console.log("port_on_error:", e);
});

parser.on('open', function (data) {
    console.log("open", data);
});

parser.on('data', function (data) {
    console.log("data", data);

    if (data === "A") {
        port.write("\r\n");
    }
});

parser.on('disconnect', function (data) {
    console.log("disconnect", data);
});

router.get('/:action', function (req, res) {
    console.log("ACTION handler triggered");
    console.log("request.url: " + req.url);
    console.log("request.baseUrl: " + req.baseUrl);
    console.log("request.path: " + req.path);
    console.log("Request URL: " + req.originalURL);
    console.log("Action triggered: " + req.params.action);

    actionHandler(req.params.action).then(function (result) {
        res.send(result);
    });
});

function actionHandler(action) {
    var result = {};

    var promise = new Promise(function (resolve, reject) {
        console.log(action + " handler triggered");

        port.write(action);

        parser.once('data', (data) => {
            console.log("get status data rx", data.toString());
            result = {
                "status": 0,
                "result": data.toString()
            };

            resolve(result);
        });

        parser.once('error', function (err) {
            result = {
                "status": -1,
                "msg": err
            };

            reject(result);
        });
    });

    return promise;
}

module.exports = router;
