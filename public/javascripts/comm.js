/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function sendAction(action, actionHandler) {
    console.log("Call action " + action);

    var xhttp = new XMLHttpRequest();
    xhttp.responseType = 'json';
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            actionHandler(xhttp);
        } else {
            console.log("ready state: " + this.readyState + ", status: " + this.status);
        }
    };

    xhttp.open("GET", "//localhost:3000/tdc_serial_comm/" + action, true);
    xhttp.send();
}

function getCoverStatus() {
    console.log("Call getStatus");

    sendAction("get_cover_status", function (xhttp) {
        var msg = "";

        console.log("ready state: " + xhttp.readyState + " status: " + xhttp.status);
        console.log("response: ", xhttp.response);

        if (xhttp.response.result === "0")
            msg = "UNKNOWN";
        else if (xhttp.response.result === "1")
            msg = "OPEN";
        else if (xhttp.response.result === "2")
            msg = "CLOSE";
        else
            msg = "ERROR!";

        document.getElementById("coverStatusContainer").innerHTML = msg;
    });
}

function getPetalStatus() {
    console.log("Call getStatus");

    sendAction("get_petal_status", function (xhttp) {
        console.log("ready state: " + xhttp.readyState + " status: " + xhttp.status);
        console.log("response: ", xhttp.response);
        document.getElementById("petalStatusContainer").innerHTML = parseInt(xhttp.response.result).toString(2).padStart(8, '0');
    });
}

function openCover() {
    console.log("Call openCover");

    sendAction("open_cover", function (xhttp) {
        console.log("ready state: " + xhttp.readyState + " status: " + xhttp.status);
        console.log("response: ", xhttp.response);
        // document.getElementById("openButton").innerHTML = xhttp.responseText;
    });
}

function closeCover() {
    console.log("Call closeCover");

    sendAction("close_cover", function (xhttp) {
        console.log("ready state: " + xhttp.readyState + " status: " + xhttp.status);
        console.log("response: ", xhttp.response);
        // document.getElementById("closeButton").innerHTML = xhttp.responseText;
    });
}

function init() {
    console.log("Call init");
    var button = document.getElementById("statusButton");

    button.addEventListener("click", getCoverStatus);
    document.getElementById("petalStatusButton").addEventListener("click", getPetalStatus);
    document.getElementById("openButton").addEventListener("click", openCover);
    document.getElementById("closeButton").addEventListener("click", closeCover);
}

window.onload = init;
