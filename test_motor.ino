#include <AccelStepper.h>
#define HALFSTEP 8

// Petal Motor pin definitions
#define motorPetal_N_Pin1  22     // IN1 on the ULN2003 driver 1
#define motorPetal_N_Pin2  24     // IN2 on the ULN2003 driver 1
#define motorPetal_N_Pin3  26     // IN3 on the ULN2003 driver 1
#define motorPetal_N_Pin4  28     // IN4 on the ULN2003 driver 1

#define motorPetal_S_Pin1  30     // IN1 on the ULN2003 driver 1
#define motorPetal_S_Pin2  32     // IN2 on the ULN2003 driver 1
#define motorPetal_S_Pin3  34     // IN3 on the ULN2003 driver 1
#define motorPetal_S_Pin4  36     // IN4 on the ULN2003 driver 1

#define motorPetal_E_Pin1  38     // IN1 on the ULN2003 driver 1
#define motorPetal_E_Pin2  40     // IN2 on the ULN2003 driver 1
#define motorPetal_E_Pin3  42     // IN3 on the ULN2003 driver 1
#define motorPetal_E_Pin4  44     // IN4 on the ULN2003 driver 1

#define motorPetal_W_Pin1  46     // IN1 on the ULN2003 driver 1
#define motorPetal_W_Pin2  48     // IN2 on the ULN2003 driver 1
#define motorPetal_W_Pin3  50     // IN3 on the ULN2003 driver 1
#define motorPetal_W_Pin4  52     // IN4 on the ULN2003 driver 1

// Limit sensor pins
#define limitOpenPetalNPin   3     // 
#define limitClosePetalNPin  4     // 
#define limitOpenPetalSPin   5     // 
#define limitClosePetalSPin  6     // 
#define limitOpenPetalEPin   7     // 
#define limitClosePetalEPin  8     // 
#define limitOpenPetalWPin   9     // 
#define limitClosePetalWPin  10    // 

#define ledPin 13 // LED pin

enum petalStatus {
    PETAL_N_IS_OPEN = 1 << 0,
    PETAL_N_IS_CLOSE = 1 << 1,
    PETAL_S_IS_OPEN = 1 << 2,
    PETAL_S_IS_CLOSE = 1 << 3,
    PETAL_E_IS_OPEN = 1 << 4,
    PETAL_E_IS_CLOSE = 1 << 5,
    PETAL_W_IS_OPEN = 1 << 6,
    PETAL_W_IS_CLOSE = 1 << 7
};

#define COVER_IS_OPEN_STATUS   (PETAL_N_IS_OPEN | PETAL_S_IS_OPEN | PETAL_E_IS_OPEN | PETAL_W_IS_OPEN)
#define COVER_IS_CLOSE_STATUS  (PETAL_N_IS_CLOSE | PETAL_S_IS_CLOSE | PETAL_E_IS_CLOSE | PETAL_W_IS_CLOSE)

enum coverStatus {
    UNKNOWN = 0,
    COVER_IS_OPEN = 1,
    COVER_IS_CLOSE = 2
};


// Initialize with pin sequence IN1-IN3-IN2-IN4 for using the AccelStepper with 28BYJ-48
AccelStepper stepper_N(HALFSTEP, motorPetal_N_Pin1, motorPetal_N_Pin3, motorPetal_N_Pin2, motorPetal_N_Pin4);
AccelStepper stepper_S(HALFSTEP, motorPetal_S_Pin1, motorPetal_S_Pin3, motorPetal_S_Pin2, motorPetal_S_Pin4);
AccelStepper stepper_E(HALFSTEP, motorPetal_E_Pin1, motorPetal_E_Pin3, motorPetal_E_Pin2, motorPetal_E_Pin4);
AccelStepper stepper_W(HALFSTEP, motorPetal_W_Pin1, motorPetal_W_Pin3, motorPetal_W_Pin2, motorPetal_W_Pin4);

// Serial buffer
String buffer;

int availableMemory() {
    int size = 8192;
    byte *buf;
    while ((buf = (byte *) malloc(--size)) == NULL);
    free(buf);
    return size;
}

void blink_on_out_of_memory() {
    if (availableMemory())
        digitalWrite(ledPin, HIGH);
    else {
        digitalWrite(ledPin, LOW);
        delay(200);
        digitalWrite(ledPin, HIGH);
        delay(200);
        digitalWrite(ledPin, LOW);
        delay(200);
        digitalWrite(ledPin, HIGH);
        delay(200);
        digitalWrite(ledPin, LOW);
        delay(200);
        digitalWrite(ledPin, HIGH);
        delay(200);
        digitalWrite(ledPin, LOW);
        delay(200);
        digitalWrite(ledPin, HIGH);
        delay(200);
        digitalWrite(ledPin, LOW);
        delay(200);
        digitalWrite(ledPin, HIGH);
        delay(200);
        digitalWrite(ledPin, LOW);
        delay(200);
        digitalWrite(ledPin, HIGH);
        delay(200);
        digitalWrite(ledPin, LOW);
        delay(200);
        digitalWrite(ledPin, HIGH);
        delay(200);
        digitalWrite(ledPin, LOW);
        delay(200);
        digitalWrite(ledPin, HIGH);
        delay(200);
    }

}

int getPetalStatus() {
    int status = 0;

    if (digitalRead(limitOpenPetalNPin))
        status |= PETAL_N_IS_OPEN;
    if (digitalRead(limitClosePetalNPin))
        status |= PETAL_N_IS_CLOSE;

    if (digitalRead(limitOpenPetalSPin))
        status |= PETAL_S_IS_OPEN;
    if (digitalRead(limitClosePetalSPin))
        status |= PETAL_S_IS_CLOSE;

    if (digitalRead(limitOpenPetalEPin))
        status |= PETAL_E_IS_OPEN;
    if (digitalRead(limitClosePetalEPin))
        status |= PETAL_E_IS_CLOSE;

    if (digitalRead(limitOpenPetalWPin))
        status |= PETAL_W_IS_OPEN;
    if (digitalRead(limitClosePetalWPin))
        status |= PETAL_W_IS_CLOSE;

    return status;
}

int getCoverStatus() {
    int status = getPetalStatus();

    if (status & COVER_IS_OPEN_STATUS)
        return COVER_IS_OPEN;
    else if (status & COVER_IS_CLOSE_STATUS)
        return COVER_IS_CLOSE;
    else
        return UNKNOWN;
}

void petalStatus() {
    if (digitalRead(limitOpenPetalNPin) || digitalRead(limitClosePetalNPin))
        digitalWrite(ledPin, HIGH);
    else {
        digitalWrite(ledPin, LOW);
    }
}

void setupPins() {
    pinMode(ledPin, OUTPUT); // Set led pin in output mode

    pinMode(limitOpenPetalNPin, INPUT_PULLUP);
    pinMode(limitClosePetalNPin, INPUT_PULLUP);
}

void setupSteppers() {
    stepper_N.setMaxSpeed(1000.0);
    stepper_S.setMaxSpeed(1000.0);
    stepper_E.setMaxSpeed(1000.0);
    stepper_W.setMaxSpeed(1000.0);

    stepper_N.setAcceleration(100.0);
    stepper_S.setAcceleration(100.0);
    stepper_E.setAcceleration(100.0);
    stepper_W.setAcceleration(100.0);

    stepper_N.setSpeed(200);
    stepper_S.setSpeed(200);
    stepper_E.setSpeed(200);
    stepper_W.setSpeed(200);

    stepper_N.moveTo(0);
    stepper_S.moveTo(0);
    stepper_E.moveTo(0);
    stepper_W.moveTo(0);
}

#define SERIAL_BOUD  115200

void setup() {
    setupPins();

    setupSteppers();

    Serial.begin(SERIAL_BOUD);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for native USB port only
    }

    while (Serial.available() <= 0) {
        Serial.println("A"); // send a capital A
        delay(300);
    }

    Serial.println("Petal driver online!"); // Send welcome message
    Serial.println("Cover STATUS: " + getCoverStatus()); // Send welcome message

    delay(1000);
}

void loop() {
    bufferParser(); // serial port handler
    petalStatus(); // Check pin status

    // Serial.println("Memory available: " + availableMemory());

    //Change direction when the stepper reaches the target position
    //    if (stepper_N.distanceToGo() == 0) {
    //        stepper_N.moveTo(-stepper_N.currentPosition());
    //    }
    stepper_N.run();
    stepper_S.run();
    stepper_E.run();
    stepper_W.run();

    //  digitalWrite(ledPin, LOW);
    //  delay(10);
}

void bufferParser() {
    if (Serial.available() > 0) {
        buffer = Serial.readString();
    }

    if (buffer.equals("")) {
        // Serial.println("No instructions received. Nothing to do");
    } else if (buffer.equals("open_cover")) {
        openCover();
    } else if (buffer.equals("close_cover")) {
        closeCover();
    } else if (buffer.equals("get_status")) {
        Serial.println("PETAL STATUS " + String(getPetalStatus())); // Send petal status
    } else {
        Serial.println("Instruction received not recognized: " + buffer);
    }

    buffer = ""; // reset buffer
}

void openCover() {
    int status = getPetalStatus();

    if (!(status & PETAL_N_IS_OPEN))
        stepper_N.moveTo(512);
    if (!(status & PETAL_S_IS_OPEN))
        stepper_S.moveTo(512);
    if (!(status & PETAL_E_IS_OPEN))
        stepper_E.moveTo(512);
    if (!(status & PETAL_W_IS_OPEN))
        stepper_W.moveTo(512);
}

void closeCover() {
    int status = getPetalStatus();

    if (!(status & PETAL_N_IS_CLOSE))
        stepper_N.moveTo(-512);
    if (!(status & PETAL_S_IS_CLOSE))
        stepper_S.moveTo(-512);
    if (!(status & PETAL_S_IS_CLOSE))
        stepper_E.moveTo(-512);
    if (!(status & PETAL_S_IS_CLOSE))
        stepper_W.moveTo(-512);
}







