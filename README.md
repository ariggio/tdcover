# Introduction #
The aim of this project is to realize a remotely controlled dust cover for a telescope.
It is divided in two parts:

* hardware: it is based on arduino (MEGA 2560) which provides control over four step-motors, each one moving a petal. The arduino code uses the library accelStepper
* software: a web interface realized using html5 and javascript, and node.js and the modules express to create a web service. The node.js part also connect to the arduino controller via serial protocol. 

## Authors

* **Alessandro Riggio**

## License

This project is licensed under the GPL License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
I want to thank Riccardo Scasseddu for very useful tips on how to start this project.